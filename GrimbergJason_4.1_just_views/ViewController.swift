//
//  ViewController.swift
//  GrimbergJason_6.2
//
//  Created by Jason Grimberg on 11/15/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    // Outlets
    @IBOutlet weak var playButtonInteraction: UIButton!
    @IBOutlet var allImages: [UIImageView]!
    @IBOutlet weak var movesLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    // Variables
    var timer = Timer()
    var timerIsOn = false
    var timeRemaining = 5
    var runCount = 0
    var currentTime = ""
    var currentSelection = 0
    var card1 = "card1"
    var card2 = "card2"
    var totalTime = ""
    
    // Private variables
    private var frontImageView: UIImageView! = nil
    private var cardImageName: String!
    private var backImageName = "back"
    private var deck = [String]()
    private var selectedIndexes = Array<Int>()
    private var numberOfPairs = 0
    private var score = 0
    private var missed = 0
    private var newGame = 1
    private var isIpad = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check what device we are using
        if UIDevice.current.userInterfaceIdiom == .pad {
            isIpad = 1
        } else {
            // Not an iPad
            isIpad = 0
        }
        
        // Start the setup
        start()
        
        for i in 0..<(deck.count) {
            // Make sure you cannot press anything while preview
            allImages[i].isUserInteractionEnabled = false
            
            // Append all of the cells so we can preview them
            selectedIndexes.append(i)
            
            // Do UI stuff
            DispatchQueue.main.async {
                // Change each imageView to the new image
                self.allImages[i].image = UIImage(named: self.backImageName)
                self.view.addSubview(self.allImages[i])
            }
        }
        // NOTE: - Look into this because this is not working currently
        // Getting the userName when the game first starts
//        showUserNameDialog()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: Start
private extension ViewController {
    func start() {
        
        deck = [String]()
        
        if isIpad != 1 {
            deck.append("Image0")
            deck.append("Image1")
            deck.append("Image2")
            deck.append("Image3")
            deck.append("Image4")
            deck.append("Image5")
            deck.append("Image6")
            deck.append("Image7")
            deck.append("Image8")
            deck.append("Image9")
        } else {
            deck.append("Image0")
            deck.append("Image1")
            deck.append("Image2")
            deck.append("Image3")
            deck.append("Image4")
            deck.append("Image5")
            deck.append("Image6")
            deck.append("Image7")
            deck.append("Image8")
            deck.append("Image9")
            deck.append("Image10")
            deck.append("Image11")
            deck.append("Image12")
            deck.append("Image13")
            deck.append("Image14")
        }
        
        // Start with a zero score
        numberOfPairs = 0
        score = 0
        missed = 0
        
        // Create the deck
        createDeck()
        
        // Shuffle the deck of images
        shuffled()
        
        // Set the new game
        newGame = 0
    }
}

// MARK: Setup
private extension ViewController {
    func setup() {
        
        for i in 0..<(deck.count) {
            
            allImages[i].isHidden = false
            
            let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                    action: #selector(imageTapped(tapGestureRecognizer:)))
            
            // Make sure the user interaction is available for all imageviews
            allImages[i].isUserInteractionEnabled = true
            allImages[i].addGestureRecognizer(tapGestureRecognizer)
            
            // Set each image for each name in the deck
            let image = UIImage(named: deck[i].description)
            
            // Add tags to all of the imageViews
            allImages[i].tag = i
            
            // Do UI stuff
            DispatchQueue.main.async {
                // Change each imageView to the new image
                self.allImages[i].image = image
                self.view.addSubview(self.allImages[i])
            }
        }
    }
}

// MARK: - Actions
private extension ViewController {
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        if selectedIndexes.count == 2 {
            return
        }
        
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        // Keep track of how many times we select something
        currentSelection += 1
        
        // Get the image that we selected and show it
        if currentSelection == 1 {
            // Add to the selected indexes on what we tapped
            selectedIndexes.append(tappedImage.tag)
            
            // Set the first card tapped
            card1 = deck[tappedImage.tag].description
            
            // Set the image to what was tapped
            let image = UIImage(named: deck[tappedImage.tag].description)
            
            // Show the image of what was just tapped
            allImages[tappedImage.tag].image = image
            
            // Disable the interaction for what was just tapped so they cannot tap it again
            allImages[tappedImage.tag].isUserInteractionEnabled = false
        } else {
            // Add to the selected indexes on what we tapped
            selectedIndexes.append(tappedImage.tag)
            
            // Set the second card tapped
            card2 = deck[tappedImage.tag].description
            
            // Set the image to what was tapped
            let image = UIImage(named: deck[tappedImage.tag].description)
            
            // Show the image of what was just tapped
            allImages[tappedImage.tag].image = image
            
            // Disable the interaction for what was just tapped so they cannot tap it again
            allImages[tappedImage.tag].isUserInteractionEnabled = false
            
            if card1 == card2 {
                // Add to the score
                score = (score + 1)
                
                // Add the number of pairs
                numberOfPairs = (numberOfPairs + 1)
                
                // Check if the game is finished
                checkIfFinished()
                
                // Remove the selected cards after 1 second
                perform(#selector(removeCards), with: nil, afterDelay: 1)
                
                // Set the current selection back to zero
                currentSelection = 0
                
                // Update the moves label
                movesLabel.text = "Moves: \(score)"
                
            } else {
                // Add to the score
                score = (score + 1)
                
                // Add to the missed score
                missed = (missed + 1)
                
                // Turn the cards back over with a delay of 1 second
                perform(#selector(turnCardsFaceDown), with: nil, afterDelay: 1)
                
                // Set the current selection back to zero
                currentSelection = 0
                
                // Update the moves label
                movesLabel.text = "Moves: \(score)"
            }
        }
    }
    
    func checkIfFinished() {
        // Check if the number of pairs is the same as the deck count
        if numberOfPairs == deck.count/2 {
            // Show the final popup
            perform(#selector(showFinalPopUp), with: nil, afterDelay: 1.1)
        }
    }
    
    // Show the final pop up
    @objc func showFinalPopUp() {
        if missed == 0 {
            // Set the total time label
            totalTime = timeLabel.text!
            
            // Set the alert type
            let alert = UIAlertController(title: "Amazing!",
                message: "You didn't miss any!\nMoves: \(score)\nTime: \(totalTime)",
                preferredStyle: UIAlertControllerStyle.alert)
            
            // Add an Ok button that will dismiss the alert
            alert.addAction(UIAlertAction(title: "Ok",
                style: .default,
                handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            
            // Present the alert after completion
            self.present(alert, animated: true, completion: nil)
            timer.invalidate()
            
        } else {
            // Set the total time label
            totalTime = timeLabel.text!
            
            // Set the alert type
            let alert = UIAlertController(title: "Job well done!",
                message: "Total Moves: \(score)\nTotal Time: \(totalTime)",
                preferredStyle: UIAlertControllerStyle.alert)
            
            // Add an Ok button that will dismiss the alert
            alert.addAction(UIAlertAction(title: "Ok",
                style: .default,
                handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            
            // Present the alert after compleltion
            self.present(alert, animated: true, completion: nil)
            timer.invalidate()
            
            // Start the setup
            start()
            
            for i in 0..<(deck.count) {
                // Make sure you cannot press anything while preview
                allImages[i].isUserInteractionEnabled = false
                
                allImages[i].isHidden = false
                
                // Append all of the cells so we can preview them
                selectedIndexes.append(i)
                
                // Do UI stuff
                DispatchQueue.main.async {
                    // Change each imageView to the new image
                    self.allImages[i].image = UIImage(named: self.backImageName)
                    self.view.addSubview(self.allImages[i])
                }
            }
        }
    }
    
    @objc func removeCards() {
        // Remove the cards at the selected indexes
        self.removeCardsAtPlaces(places: self.selectedIndexes)
        
        // Clear the array after selections were made
        self.selectedIndexes = Array<Int>()
    }
    
    func removeCardsAtPlaces(places: Array<Int>) {
        // Hide the cards that match
        for i in places {
            allImages[i].isHidden = true
        }
    }
    
    func createDeck() {
        // Create a deck with all of our cards to make a full deck of images x2
        let halfDeck = deck
        return deck = (halfDeck + halfDeck)
    }
    
    func shuffled(){
        // Temp list w/ both halfs of our deck
        var list = deck
        
        // Shuffle the temp deck using the old arc4random
        for i in 0..<(list.count - 1) {
            let j = Int(arc4random_uniform(UInt32(list.count - i))) + i
            list.swapAt(i, j)
        }
        // Retrun the shuffled deck
        return deck = list
    }
    
    @objc func turnCardsFaceDown() {
        // Turn over the selected cards
        self.downturnCardsAtPlaces(places: self.selectedIndexes)
        
        // Clear the array after selections were made
        self.selectedIndexes = Array<Int>()
    }
    
    func downturnCardsAtPlaces(places: Array<Int>) {
        for i in places {
            // Add user interaction back to true on the cards selected
            allImages[i].isUserInteractionEnabled = true
            
            // 'Flip' the cards back over
            allImages[i].image = UIImage(named: backImageName)
        }
    }
    
    @objc func timerRunning() {
        
        if timeRemaining != 0 {
            timeRemaining -= 1
            
            // Set the minutes and seconds
            let minutesLeft = Int(timeRemaining) / 60 % 60
            let secondsLeft = Int(timeRemaining) % 60
            
            // Show/update the time
            timeLabel.text = "0\(minutesLeft):0\(secondsLeft)"
        } else {
            // Set the play button user interaction
            playButtonInteraction.isUserInteractionEnabled = true
            
            // Set the timer to true
            timerIsOn = true
            runCount += 1
            
            // Show the minutes and seconds
            let minutesLeft = Int(runCount) / 60 % 60
            let secondsLeft = Int(runCount) % 60
            
            // Show the time label
            timeLabel.text = "\(String(format: "%02d", minutesLeft)):\(String(format: "%02d", secondsLeft))"
        }
    }
    
    func showUserNameDialog() {
        // Creating UIAlertController and
        // Setting title and message for the alert dialog
        let alertController = UIAlertController(title: "Enter details?", message: "Enter your name", preferredStyle: .alert)
        
        // Ghe confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Enter", style: .default) { (_) in
            
            // Getting the input values from user
            let name = alertController.textFields?[0].text
            
            // Test for the user name
            print("Name: " + name!)
            
        }
        
        // The cancel action doing nothing
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        // Adding textfields to our dialog box
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter Name"
        }
        
        // Adding the action to dialogbox
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        // Finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - Segues
    // Unwind to the root screen
    @IBAction func unwindToRoot(segue: UIStoryboardSegue) {}
    
    // MARK: - Button Actions
    @IBAction func playPressed(_ sender: UIButton) {
        runCount = 0
        score = 0
        timerIsOn = false
        timeRemaining = 5
        timeLabel.text = "00:05"
        movesLabel.text = "Moves: \(score)"
        timer.invalidate()
        playButtonInteraction.setTitle("Reset", for: .normal)
        
        if !timerIsOn {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self,
                                         selector: #selector(timerRunning), userInfo: nil, repeats: true)
            // Keep the timer on until 0
            timerIsOn = true
        }
        
        // Start the program
        start()
        
        // Setup the view
        setup()
        
        // Turn off the user interaction on the button
        playButtonInteraction.isUserInteractionEnabled = false
        
        for i in 0..<(deck.count) {
            // Make sure you cannot press anything while preview
            allImages[i].isUserInteractionEnabled = false
            
            // Append all of the cells so we can preview them
            selectedIndexes.append(i)
            
            // Flip all of the cards back over after 5 seconds
            perform(#selector(turnCardsFaceDown), with: nil, afterDelay: TimeInterval(timeRemaining))
        }
    }
}
