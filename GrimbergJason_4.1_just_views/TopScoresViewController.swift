//
//  TopScoresViewController.swift
//  GrimbergJason_6.2
//
//  Created by Jason Grimberg on 11/15/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit
import CoreData

class TopScoresViewController: UIViewController {

    /* ManagedObjectContext - Our Notepad. We write on the notepad and then save the notepad to the device. It's our Data Middle Man between our code and the Hard Drive. */
    private var managedContext:NSManagedObjectContext!
    
    /* NSEntityDescription - Used to help build our Entity by describing a specific Entity from our .xcdatamodeld file. */
    private var entityDescription:NSEntityDescription!
    
    /* NSManagedObject - Used to represent the Entity Type 'NumTaps' that we created in our .xcdatamodeld file.
     We use the entity description to help us build the right kind of entity. */
    /* This is where our data lives. Everything else is just setup. */
    private var userScore:NSManagedObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: Any) {
        performSegue(withIdentifier: "unwindToRoot", sender: self)
    }

    func loadTopScores() {
        
        /* We use Fetch Requests to get the data we want off of the 'notepad' */
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "NumTaps")
        
        do {
            let results:[NSManagedObject] = try managedContext.fetch(fetchRequest)
            
            /* Update our stored property with what we've found on the hard drive */
            
            for obj in results {
                print(obj.description)
                
                userScore = obj
            }
        }
        catch {
            print("Load failed")
        }
        
        //Update UI
        if userScore != nil {
            //count = userScore.value(forKey: "userScore") as! Int
        } else {
            /* Use the description to make an NSManagedObject of 'NumTaps' type */
            userScore = NSManagedObject(entity: entityDescription, insertInto: managedContext)
        }
        // NOTE: - Set the labels with the data/scores
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
